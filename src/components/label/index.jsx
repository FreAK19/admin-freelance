import React from 'react';

import './styles.css';

const Label = ({ children, required }) => (
  <label className="form-label">
    {children} {required && '*'}
  </label>
);

export default Label;
