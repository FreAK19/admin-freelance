import React, { useState } from 'react';
import MaskedInput from 'react-text-mask';

const PHONE_MASK = [
  '+',
  '3',
  '8',
  ' ',
  '(',
  '0',
  /[0-9]/,
  /[0-9]/,
  ')',
  ' ',
  /[0-9]/,
  /[0-9]/,
  /[0-9]/,
  '-',
  /[0-9]/,
  /[0-9]/,
  '-',
  /[0-9]/,
  /[0-9]/
];
const phoneNumberMuskFormat = '+38 (0__) __-__-__';

const PhoneControl = ({ value, onChange, placeholder = '' }) => {
  const [isFocused, changeFocus] = useState(false);

  const handleChange = event => {
    const target = event.target;

    if (target !== null && target.value) {
      const value = target.value;
      onChange(value);
    }
  };

  const placeholderText = isFocused ? phoneNumberMuskFormat : placeholder;

  return (
    <MaskedInput
      type="tel"
      guide
      required
      onFocus={() => changeFocus(true)}
      onBlur={() => changeFocus(false)}
      value={value}
      mask={PHONE_MASK}
      placeholder={placeholderText}
      onChange={handleChange}
      showMask
      className="ant-input"
    />
  );
};

export default PhoneControl;
