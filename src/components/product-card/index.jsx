import React from 'react';
import Card from 'antd/lib/card';
import Checkbox from 'antd/lib/checkbox';
import Collapse from 'antd/lib/collapse';

import { EditOutlined, CaretRightOutlined } from '@ant-design/icons';

import './styles.css';

const { Meta } = Card;
const { Panel } = Collapse;

const ProductCard = ({
  card,
  onEdit,
  onSelect,
  openPannelId,
  onOpenPannel
}) => {
  return (
    <div className="card-container">
      <Checkbox className="delete-checkbox" onChange={onSelect} />
      <Card
        className="card"
        cover={
          <img
            alt="example"
            src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
          />
        }
        actions={[<EditOutlined key="edit" onClick={() => onEdit(card)} />]}
      >
        <Meta title={card.name} />
        <Meta title={`Price: ${card.price} $`} />
        <Collapse
          activeKey={openPannelId}
          className="collapse-custom-card"
          bordered={false}
          onChange={onOpenPannel}
          expandIcon={({ isActive }) => (
            <CaretRightOutlined rotate={isActive ? 90 : 0} />
          )}
        >
          <Panel header="Read more" key={card.id}>
            <p>{card.description}</p>
          </Panel>
        </Collapse>
      </Card>
    </div>
  );
};

export default ProductCard;
