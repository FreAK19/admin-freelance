import React, { useCallback } from 'react';
import Modal from 'antd/lib/modal';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Button from 'antd/lib/button';
import InputNumber from 'antd/lib/input-number';

import Label from '../label';

import './styles.css';

const { TextArea } = Input;

const ProductModal = ({ product, onSubmit, onCancel, open, loading }) => {
  const handleSubmit = useCallback(
    values => {
      onSubmit(product.id, values);
    },
    [onSubmit, product]
  );

  return (
    <Modal
      title={product.id ? 'Edit product' : 'Create product'}
      visible={open}
      confirmLoading={loading}
      onCancel={onCancel}
      footer={[]}
    >
      {product.name && (
        <>
          <Form
            name="productModal"
            onFinish={handleSubmit}
            initialValues={{ ...product }}
          >
            <Label required>Name</Label>
            <Form.Item
              name="name"
              rules={[
                {
                  required: true,
                  message: 'Please input product name!'
                }
              ]}
            >
              <Input placeholder="Name" maxLength="255" />
            </Form.Item>

            <Label required>Price</Label>
            <Form.Item
              name="price"
              rules={[
                {
                  required: true,
                  message: 'Please input product price!'
                }
              ]}
            >
              <InputNumber
                defaultValue={100}
                min={0}
                formatter={value =>
                  `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                }
                parser={value => value.replace(/\$\s?|(,*)/g, '')}
              />
            </Form.Item>

            <Label required>Description</Label>
            <Form.Item
              name="description"
              rules={[
                {
                  required: true,
                  message: 'Please input product description!'
                }
              ]}
            >
              <TextArea placeholder="Description" maxLength="1000" rows={4} />
            </Form.Item>

            <div className="modal-actions">
              <Button onClick={onCancel} className="cancel-modal-btn">
                Cance
              </Button>
              <Button type="primary" htmlType="submit" loading={loading}>
                Save
              </Button>
            </div>
          </Form>
        </>
      )}
    </Modal>
  );
};

export default ProductModal;
