import React, { useCallback } from 'react';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Button from 'antd/lib/button';
import Collapse from 'antd/lib/collapse';
import notification from 'antd/lib/notification';

import Label from './../../../components/label';
import PhoneInput from './../../../components/phone-input';

const initialValues = {
  firstName: '',
  lastName: '',
  email: '',
  mobile: '',
  remember: true
};

const { Panel } = Collapse;

const UserSetting = () => {
  const openSuccessNotification = useCallback(() => {
    notification.open({
      message: 'Form updated',
      description: 'User profile was successfully updated!'
    });
  }, []);

  const handleFormSubmit = useCallback(() => {
    openSuccessNotification();
  }, [openSuccessNotification]);

  return (
    <>
      <h2>User setting</h2>
      <Form
        name="userSetting"
        initialValues={initialValues}
        onFinish={handleFormSubmit}
      >
        <Row gutter={16}>
          <Col span={12}>
            <Label>First Name</Label>
            <Form.Item name="firstName">
              <Input maxLength="255" placeholder="First name" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Label>Last Name</Label>
            <Form.Item name="lastName">
              <Input maxLength="255" placeholder="Last name" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Label required>Email</Label>
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: 'Please input your email!'
                }
              ]}
            >
              <Input type="email" placeholder="Email" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Label>Phone Number</Label>
            <Form.Item name="mobile">
              <PhoneInput placeholder="Phone number" />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item>
          <Button type="primary" htmlType="submit" block size="large">
            Save
          </Button>
        </Form.Item>
      </Form>
      <Collapse>
        <Panel header="Reset password" key="1">
          <h2>Reset password</h2>
          <Form name="resetPassword" onFinish={handleFormSubmit}>
            <Row gutter={16}>
              <Col span={12}>
                <Label>Password</Label>
                <Form.Item
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: 'Please input your password!'
                    },
                    {
                      min: 6,
                      message: 'Password must be minimum 6 characters.'
                    },
                    {
                      max: 16,
                      message: 'Password can be maximum 16 characters.'
                    }
                  ]}
                >
                  <Input.Password placeholder="New password" />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={12}>
                <Label>Repeat Password</Label>
                <Form.Item
                  name="repeatPassword"
                  rules={[
                    {
                      required: true,
                      message: 'Please input your password!'
                    },
                    {
                      min: 6,
                      message: 'Password must be minimum 6 characters.'
                    },
                    {
                      max: 16,
                      message: 'Password can be maximum 16 characters.'
                    }
                  ]}
                >
                  <Input.Password placeholder="Repeat password" />
                </Form.Item>
              </Col>
            </Row>

            <Form.Item>
              <Button type="primary" htmlType="submit" block size="large">
                Reset password
              </Button>
            </Form.Item>
          </Form>
        </Panel>
      </Collapse>
    </>
  );
};

export default UserSetting;
