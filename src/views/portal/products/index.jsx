import React, { useEffect, useCallback, useState } from 'react';
import axios from 'axios';
import Button from 'antd/lib/button';
import Empty from 'antd/lib/empty';
import Pagination from 'antd/lib/pagination';
import PageHeader from 'antd/lib/page-header';
import ProductCard from './../../../components/product-card';
import ProductModal from '../../../components/product-modal';

import './styles.css';

const baseUrl = 'http://localhost:8080';

const ProductsList = () => {
  const [page, setPage] = useState(0);
  const [loading, setLoading] = useState(false);
  const [products, setProducts] = useState([]);
  const [currentProduct, setCurrentProduct] = useState({});
  const [activeOpenPannelId, changeActiveOpenPannelId] = useState(['0']);
  const [deletedProductIds, setDeletedProductIds] = useState([]);
  const [productModalOpen, changeProductModalOpen] = useState(false);

  useEffect(() => {
    fetchPosts();
  }, []);

  const fetchPosts = useCallback(
    async (page = 0, size = 10) => {
      try {
        const response = await axios.get(
          `${baseUrl}/products?_start=${page}&_limit=${size}`
        );

        setPage(page);
        setProducts(response.data);
      } catch (err) {
        throw err;
      }
    },
    [setProducts, setPage]
  );

  const handleSubmitProduct = useCallback(
    async (productId, values) => {
      setLoading(true);

      try {
        if (productId) {
          await axios.put(`${baseUrl}/products/${productId}`, values);
        } else {
          await axios.post(`${baseUrl}/products`, values);
        }
        await fetchPosts();
        setCurrentProduct({ name: ' ' });
        changeProductModalOpen(false);
      } catch (err) {
        throw err;
      } finally {
        setLoading(false);
      }
    },
    [fetchPosts, setLoading]
  );

  const handleDeleteProducts = useCallback(async () => {
    try {
      await Promise.all(
        deletedProductIds.map(async id => {
          await axios.delete(`${baseUrl}/products/${id}`);
        })
      );
      setDeletedProductIds([]);
      await fetchPosts();
    } catch (err) {
      throw err;
    }
  }, [fetchPosts, deletedProductIds, setDeletedProductIds]);

  const handleCreateNewProduct = useCallback(() => {
    changeProductModalOpen(true);
    setCurrentProduct({ name: ' ' });
  }, [setCurrentProduct, changeProductModalOpen]);

  const handleEditProducts = useCallback(
    product => {
      setCurrentProduct(product);
      changeProductModalOpen(true);
    },
    [setCurrentProduct, changeProductModalOpen]
  );

  const handleSelectDeletedIds = useCallback(
    (productId, index) => {
      const copyDeletedIds = [...deletedProductIds];

      if (copyDeletedIds.includes(productId)) {
        copyDeletedIds.splice(index, 1);
      } else {
        copyDeletedIds.push(productId);
      }

      setDeletedProductIds(copyDeletedIds);
    },
    [setDeletedProductIds, deletedProductIds]
  );

  const handleChangePannel = useCallback(
    productId => {
      let pannelIds = [...activeOpenPannelId];

      if (pannelIds.includes(productId)) {
        pannelIds = ['0'];
      } else {
        pannelIds = [productId];
      }

      changeActiveOpenPannelId(pannelIds);
    },
    [changeActiveOpenPannelId, activeOpenPannelId]
  );

  return (
    <>
      <div className="page-header-container">
        <PageHeader title="Products" subTitle="Total products - 100" />
        <div>
          {deletedProductIds.length > 0 && (
            <Button
              type="primary"
              className="delete-btn"
              danger
              onClick={handleDeleteProducts}
            >
              Delete products
            </Button>
          )}
          <Button type="primary" danger onClick={handleCreateNewProduct}>
            Create product
          </Button>
        </div>
      </div>

      {products.length > 0 ? (
        <div className="products-list">
          {products.map((item, index) => {
            return (
              <ProductCard
                key={item.id}
                card={item}
                onEdit={handleEditProducts}
                openPannelId={activeOpenPannelId}
                onOpenPannel={() => handleChangePannel(item.id)}
                onSelect={() => handleSelectDeletedIds(item.id, index)}
              />
            );
          })}
        </div>
      ) : (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
      )}

      <Pagination
        total={100}
        showSizeChanger
        defaultCurrent={page}
        onChange={fetchPosts}
      />

      <ProductModal
        loading={loading}
        product={currentProduct}
        open={productModalOpen}
        onSubmit={handleSubmitProduct}
        onCancel={() => {
          changeProductModalOpen(false);
          setCurrentProduct({});
        }}
      />
    </>
  );
};

export default ProductsList;
