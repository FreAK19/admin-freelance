import React, { useEffect, useState, useCallback, useMemo } from 'react';
import { Switch, Route, NavLink, useHistory } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  LogoutOutlined
} from '@ant-design/icons';

import Logo from './../../components/logo';
import UserSetting from './setting';
import ProductsList from './products';

import './styles.css';

const { Header, Sider, Content } = Layout;

const Portal = () => {
  const [collapsed, setCollapsed] = useState(false);
  const history = useHistory();
  const user = useMemo(() => {
    return JSON.parse(
      localStorage.getItem('user') || sessionStorage.getItem('user')
    );
  }, []);

  useEffect(() => {
    history.push('/portal/products');
  }, [history]);

  const logout = useCallback(() => {
    localStorage.clear();
    history.push('/');
  }, [history]);

  return (
    <Layout className="portal-container">
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <Logo />
        <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
          <Menu.Item key="1">
            <NavLink to="/portal/products" activeClassName="active">
              <VideoCameraOutlined />
              <span>Products</span>
            </NavLink>
          </Menu.Item>
          <Menu.Item key="2">
            <NavLink to="/portal/user-setting" activeClassName="active">
              <UserOutlined />
              <span className="portal-menu-link">User Profile</span>
            </NavLink>
          </Menu.Item>
          <Menu.Item key="3" onClick={logout}>
            <LogoutOutlined />
            <span className="portal-menu-link">Logout</span>
          </Menu.Item>
        </Menu>
      </Sider>

      <Layout>
        <Header className="app-header">
          {React.createElement(
            collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
            {
              className: 'trigger',
              onClick: () => setCollapsed(!collapsed)
            }
          )}

          <h3 className="header-user-name">{user ? user.email : ''}</h3>
        </Header>
        <Content className="app-layout portal-content">
          <Switch>
            <Route path="/portal/products" exact>
              <ProductsList />
            </Route>
            <Route path="/portal/user-setting" exact>
              <UserSetting />
            </Route>
          </Switch>
        </Content>
      </Layout>
    </Layout>
  );
};

export default Portal;
