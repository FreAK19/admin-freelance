import React from 'react';
import Result from 'antd/lib/result';
import Button from 'antd/lib/button';

import { useHistory } from 'react-router-dom';

const NotFoundPage = () => {
  const history = useHistory();

  return (
    <Result
      status="404"
      title="404"
      subTitle="Sorry, the page you visited does not exist."
      extra={
        <Button type="primary" onClick={() => history.push('/')}>
          Back Home
        </Button>
      }
    />
  );
};

export default NotFoundPage;
