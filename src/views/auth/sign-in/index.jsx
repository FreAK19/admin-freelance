import React, { useCallback, useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Layout from 'antd/lib/layout';
import Button from 'antd/lib/button';
import Checkbox from 'antd/lib/checkbox';
import { useHistory } from 'react-router-dom';

import Label from './../../../components/label';

import './styles.css';

const { Content } = Layout;

const initialValues = {
  remember: true
};

const SignInPage = () => {
  const history = useHistory();
  const user = useMemo(() => {
    return JSON.parse(
      localStorage.getItem('user') || sessionStorage.getItem('user')
    );
  }, []);

  const onFinish = useCallback(
    values => {
      const store = values.remember ? localStorage : sessionStorage;
      store.setItem('user', JSON.stringify(values));
      history.push('/portal');
    },
    [history]
  );

  if (user && user.email) {
    return <Redirect to="/portal" />;
  }

  return (
    <div className="login-container">
      <Content className="form-page">
        <h2 className="form-page-title">Sign in</h2>
        <Form name="signIn" onFinish={onFinish} initialValues={initialValues}>
          <Label required>Email</Label>
          <Form.Item
            name="email"
            type="email"
            rules={[
              {
                required: true,
                message: 'Please input your email!'
              }
            ]}
          >
            <Input placeholder="Email" type="email" />
          </Form.Item>

          <Label required>Password</Label>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!'
              },
              { min: 6, message: 'Password must be minimum 6 characters.' },
              { max: 16, message: 'Password can be maximum 16 characters.' }
            ]}
          >
            <Input.Password placeholder="Password" />
          </Form.Item>

          <Form.Item name="remember" valuePropName="checked">
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" block size="large">
              Sign in
            </Button>
          </Form.Item>
        </Form>
      </Content>
    </div>
  );
};

export default SignInPage;
