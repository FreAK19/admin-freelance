import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

import DashboardPage from './views/portal';
import NotFoundPage from './views/NotFoundPage';
import SingInPage from './views/auth/sign-in';

import './App.css';

function App() {
  return (
    <Router>
      <div className="app">
        <Switch>
          <Route path="/" exact>
            <SingInPage />
          </Route>
          <Route path="/portal">
            <DashboardPage />
          </Route>

          <Route path="/404" component={NotFoundPage} />
          <Route path="*" component={NotFoundPage} />
          <Redirect to="/404" />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
